[x.dm.slick-greeter]
theme-name='Mint-X'
icon-theme-name='soulmate'
font-name='Ubuntu 11'
background='/usr/share/backgrounds/soulmate/soulmate-default.jpg'
draw-user-backgrounds=true

[org.onboard]
theme='/usr/share/mint-artwork/onboard/Nightshade.theme'
key-label-overrides=['RWIN:Super:', 'LWIN:Super:']

[org.onboard.theme-settings]
key-label-overrides=['RWIN:Super:', 'LWIN:Super:']

[org.gnome.desktop.interface]
cursor-theme='DMZ-White'
document-font-name="Noto Sans 10"
font-name="Noto Sans 9"
gtk-key-theme='Default'
gtk-theme='Mint-X-Red'
icon-theme='soulmate'
monospace-font-name="Monospace 10"
toolkit-accessibility=false

[org.gnome.desktop.background]
color-shading-type="solid"
picture-options="zoom"
picture-uri='file:///usr/share/backgrounds/soulmate/soulmate-default.jpg'
primary-color="#000000"
secondary-color="#000000"
show-desktop-icons=false

[org.gnome.desktop.wm.preferences]
button-layout=':minimize,maximize,close'
num-workspaces=4
theme='Menta'
titlebar-font="Noto Sans Bold 10"
titlebar-uses-system-font=false

[org.gnome.desktop.sound]
event-sounds=false
input-feedback-sounds=false
theme-name="LinuxMint"

[org.gnome.settings-daemon.plugins.xsettings]
hinting="slight"

[org.cinnamon.desktop.interface]
cursor-theme='DMZ-White'
gtk-key-theme='Default'
gtk-theme='Mint-X-Red'
icon-theme='soulmate'
menus-have-icons=true
toolkit-accessibility=false
font-name="Noto Sans 9"
scaling-factor=uint32 0

[org.cinnamon.desktop.screensaver]
font-date="Noto Sans 24"
font-message="Noto Sans 14"
font-time="Noto Sans 64"

[org.cinnamon.desktop.background]
primary-color="#000000"
secondary-color="#000000"
color-shading-type="solid"
picture-uri='file:///usr/share/backgrounds/soulmate/soulmate-default.jpg'
picture-options="zoom"

[org.cinnamon.desktop.wm.preferences]
button-layout=':minimize,maximize,close'
num-workspaces=4
theme='Menta'
titlebar-font="Noto Sans Bold 10"
titlebar-uses-system-font=false

[org.cinnamon.settings-daemon.plugins.xsettings]
hinting="slight"
menus-have-icons=true

[org.cinnamon.settings-daemon.plugins.power]
button-power="interactive"
lock-on-suspend=true

[org.cinnamon.theme]
name='soulmate-default'

[org.cinnamon]
desktop-effects-minimize-effect="traditional"
panel-scale-text-icons=true
workspace-expo-view-as-grid=true
panels-enabled=['1:0:top', '2:0:bottom']
enabled-applets=['panel1:left:0:menu@cinnamon.org', 'panel1:left:1:show-desktop@cinnamon.org', 'panel1:left:2:panel-launchers@cinnamon.org', 'panel1:right:3:notifications@cinnamon.org', 'panel1:right:4:user@cinnamon.org', 'panel1:right:5:removable-drives@cinnamon.org', 'panel1:right:6:keyboard@cinnamon.org', 'panel1:right:7:network@cinnamon.org', 'panel1:right:8:sound@cinnamon.org', 'panel1:right:9:power@cinnamon.org', 'panel1:right:10:calendar@cinnamon.org', 'panel1:right:11:windows-quick-list@cinnamon.org', 'panel2:right:12:trash@cinnamon.org', 'panel2:right:13:workspace-switcher@cinnamon.org', 'panel2:left:14:window-list@cinnamon.org']

[org.cinnamon.sounds]
close-file="/usr/share/mint-artwork/sounds/close.oga"
login-file="/usr/share/mint-artwork/sounds/login.oga"
logout-file="/usr/share/mint-artwork/sounds/logout.ogg"
map-file="/usr/share/mint-artwork/sounds/map.oga"
maximize-file="/usr/share/mint-artwork/sounds/maximize.oga"
minimize-file="/usr/share/mint-artwork/sounds/minimize.oga"
notification-file="/usr/share/mint-artwork/sounds/notification.oga"
plug-file="/usr/share/mint-artwork/sounds/plug.oga"
switch-file="/usr/share/mint-artwork/sounds/switch.oga"
tile-file="/usr/share/mint-artwork/sounds/tile.oga"
unmaximize-file="/usr/share/mint-artwork/sounds/unmaximize.oga"
unplug-file="/usr/share/mint-artwork/sounds/unplug.oga"
close-enabled=false
login-enabled=true
logout-enabled=true
map-enabled=false
maximize-enabled=false
minimize-enabled=false
notification-enabled=false
plug-enabled=true
switch-enabled=true
tile-enabled=true
unmaximize-enabled=false
unplug-enabled=true

[org.cinnamon.desktop.sound]
event-sounds=false
input-feedback-sounds=false
theme-name="LinuxMint"
volume-sound-enabled=true
volume-sound-file="/usr/share/mint-artwork/sounds/volume.oga"

[org.cinnamon.desktop.session]
session-manager-uses-logind=true
settings-daemon-uses-logind=true

[org.nemo.desktop]
computer-icon-visible=true
font="Noto Sans 10"
home-icon-visible=true
volumes-visible=true

[org.nemo.window-state]
start-with-status-bar=true

[org.nemo.preferences]
thumbnail-limit=4294967295

[org.mate.interface]
buttons-have-icons=false
document-font-name="Noto Sans 10"
enable-animations=false
font-name="Noto Sans 9"
gtk-theme="Mint-X-Red"
icon-theme="soulmate"
menus-have-icons=true
monospace-font-name="Monospace 10"

[org.mate.background]
color-shading-type="solid"
picture-filename="/usr/share/backgrounds/soulmate/soulmate-default.jpg"
picture-options="zoom"
primary-color="#000000000000"
secondary-color="#000000000000"
show-desktop-icons=true

[org.mate.font-rendering]
antialiasing="rgba"
dpi=96.0
hinting="slight"
rgba-order="rgb"

[org.mate.sound]
theme-name="LinuxMint"

[org.mate.caja.desktop]
computer-icon-visible=true
font="Noto Sans 10"
home-icon-visible=true
trash-icon-visible=false
volumes-visible=true

[org.mate.caja.preferences]
background-color="#ffffff"
background-set=false
enable-delete=true
preview-sound="never"
show-icon-text="never"
show-image-thumbnails="always"
thumbnail-limit=4294967295

[org.mate.Marco.general]
button-layout=":minimize,maximize,close"
center-new-windows=true
compositing-manager=true
num-workspaces=4
theme="Menta"

[org.mate.NotificationDaemon]
popup-location="top_right"
theme="coco"

[org.mate.peripherals-touchpad]
natural-scroll=true
tap-to-click=true

[org.mate.peripherals-keyboard-xkb.kbd]
options=['terminate\tterminate:ctrl_alt_bksp']

[org.mate.peripherals-mouse]
cursor-theme="default"

[org.mate.Marco.general]
titlebar-font="Noto Sans Bold 10"

[org.mate.Marco.global-keybindings]
run-command-terminal="<Primary><Alt>t"

[org.mate.power-manager]
notify-low-capacity=false

[org.mate.panel]
default-layout='soulmate'

[org.mate.mate-menu]
applet-icon="/usr/share/pixmaps/soulmate-artwork/soulmate-mate-logo.svg"

[org.mate.applications-at-mobility]
exec='onboard'

[org.gtk.Settings.FileChooser]
sort-directories-first=true
